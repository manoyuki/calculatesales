package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String FILE_NOT_DIGITS = "合計金額が10桁を超えました";
	private static final String FILE_NOT_BRANCH = "の支店コードが不正です";
	private static final String FILE_NOT_COMMODITY = "の商品コードが不正です";
	private static final String FILE_SALE_INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//エラー処理（コマンドライン引数）
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		String branchCharacter = "^\\d{3}$";
		String commodityCharacter = "^[0-9a-zA-Z]{8}$";

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchCharacter)) {
			return;
		}

		//商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, commodityCharacter)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {

			//エラー処理（ファイルなのかの確認）
			//	売上ファイル名のみ表示
			if (files[i].isFile() && files[i].getName().matches("^\\d{8}+.rcd$")) {
				rcdFiles.add(files[i]);
			}

		}
		//エラー処理（売上集計ファイル連番）
		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(files[i].getName().substring(0, 8));
			int latter = Integer.parseInt(files[i + 1].getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		//売上ファイルパス
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String line;

				//ストリング格納するリスト作る
				ArrayList<String> saleList = new ArrayList<>();

				while ((line = br.readLine()) != null) {
					//ラインのデータをリストに格納
					saleList.add(line);
				}
				//エラー処理（売上ファイルフォーマット）
				if (saleList.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FILE_SALE_INVALID_FORMAT);
					return;
				}
				//				//エラー処理（金額が数字なのか）
				if (!(saleList.get(2)).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//エラー処理（支店コード不正）
				if (!branchSales.containsKey(saleList.get(0))) {
					System.out.println(files[i].getName() + FILE_NOT_BRANCH);
					return;
				}

				//エラー処理
				if (!commoditySales.containsKey(saleList.get(1))) {
					System.out.println(files[i].getName() + FILE_NOT_COMMODITY);
					return;
				}

				//ここから集計
				long fileSale = Long.parseLong(saleList.get(2));

				//売上ファイルの金額
				Long saleAmount = branchSales.get(saleList.get(0)) + fileSale;
				Long commodityAmount = commoditySales.get(saleList.get(1)) + fileSale;

				//エラー処理（売上金額桁数）
				if (saleAmount >= 10000000000L || commodityAmount >= 10000000000L) {
					System.out.println(FILE_NOT_DIGITS);
					return;
				}

				branchSales.put(saleList.get(0), saleAmount);
				commoditySales.put(saleList.get(1), commodityAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> fileNames,
			Map<String, Long> fileSales, String commodityFormat) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//（エラー処理）ファイルの存在有無
			if (!file.exists()) {
				System.out.println(file.getName() + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)

				String[] items = line.split(",");
				//エラー処理(支店ファイルフォーマット）
				if ((items.length != 2) || (!items[0].matches(commodityFormat))) {
					System.out.println(FILE_INVALID_FORMAT);
					return false;
				}
				fileNames.put(items[0], items[1]);
				fileSales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> fileNames,
			Map<String, Long> fileSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key : fileSales.keySet()) {
				String name = fileNames.get(key);
				Long sale = fileSales.get(key);
				bw.write(key + "," + name + "," + sale);
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}